# Dodge Them!
Dodge obstacles and aim for the highest score!<br/>
In this game there are 12 secret achivements. Will you be able to find out them all?
In addition, obtain all achivements to unlock a new feature!
</br></br>
You can play both on PC and on Smartphone (better experience on PC). On Smartphone you need to use a browser and not a HTML viewer.
<br/><br/>
Try it online at https://drill.gitlab.io/HTML5_Games-DodgeThem-
</br></br>

## Commands
There are 2 types of commands:
<ul>
  <li><b>Keyboard</b>: use the directional arrows to move the sprite.</li>
  <li><b>Buttons</b>: click/tap the buttons to move the sprite (better for touchscreen devices).</li>
</ul>
</br>

## Screenshots
![DodgeThem__screenshot1](https://gitlab.com/Drill/HTML5_Games-DodgeThem-/uploads/392e997d86eb109cbe62fe272566aa65/DodgeThem__screenshot1.PNG)
![DodgeThem__screenshot2](https://gitlab.com/Drill/HTML5_Games-DodgeThem-/uploads/424c76c474831db515e2b11501a0569f/DodgeThem__screenshot2.PNG)
<br/><br/>

## Credits
Thanks to Fabio Prizio to have drawn the sprite, the flames and the sprite's broken layer!<br/>
Check out the `.html` file to see all other credits.
<br/>
<br/>

---


<i>Note</i>: if you change the files position, you'll need to update the relative paths in the `.html` file.
